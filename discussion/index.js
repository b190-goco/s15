// commenting in JS

/*CODE*/

// alert()
	//	- allows browser to display message inside ""

console.log("Hello World")

console.
log
(
	"Hello Again"
);

/*VARIABLES*/
/*
	- used to store data
	- information stored (memory)
	- portion of the memory's name(variable)
*/
/*INITIALIZING VARIABLE*/
	// - let/const keyword
		// let myVariable;
	// - called but not declared>'not defined'
	// - initialized = ready to receive data
		// no value assigned>'undefined'
let myVariable ="Hello";

console.log(myVariable)
/*
	GUIDES IN WRITING VARIABLES
		- using right keyword for initialize of variable (let/const)
		- variable name rules
			. start with lowercase character (camel casing)
				- small first word, title next words

*/

// SAMPLE VARIABLE GUIDES

/*let firstName = "Jygs";
let pokemon = 25000; // bad: what is 25000? 
let pokemon = 25000; // bad: 25000 is number; did not log 
let pika = pokemon-pokemon; // works with pokemon
console.log(pika);

let FirstName = "Jygs"; // bad: capital first
let firstName = "Jygs";

let first name = "Jygs"; // bad: spaced, use _

let first-name = "Jygs"; // bad: hyphen is subtract

use camelCasing
	- lastName
	- emailAddress
	- mobileNumber
*/

let productName = "Desktop Computer";
console.log(productName);

/*MINI ACTIVITY*/
// Create a productPrce with the value 18999 and log it in the console

let productPrice = 18999;
console.log(productPrice);

const interest = 3.539;
console.log(interest);

/*REASSIGNING VARIABLE VALUES*/
// change initial/previous value with another
	//SYNTAX: variableName = newValue;
productName="Laptop";
console.log(productName);

/*MINI ACTIVITY*/
// create a "friend" variable and assign a name to it (log)
// reassign a new name to "friend" (log)

let friendName = "Monica";
console.log(friendName);
friendName = "Chandler";
console.log(friendName);

//can't 'let' existing variable

/*CONSTANT VARIABLE*/
// no changing a const variable (reassign/reinit)
/*const interest = 4.489; // error: trying to change CONSTANT
console.log(interest);*/
// used when var will change

/*REASSIGNING vs. INITIALIZING*/
let supplier; //initialiize
/*console.log(supplier);*/
supplier = "Rachel"; //reassign
console.log(supplier);
supplier = "Zuitt Store";
console.log(supplier);

/*VAR vs. LET/CONST*/
// var
	// used in declaring variable
	// ECMAScript1 (ES1) feature [ES1 (JavaScript 1997)]
// let/const
	// introduced as new feature of ES6 (2015)
// DIFFERENCE
	// issues with var reg. hoisting
	// var is hoisted
		// HOISTING
			// JS's behaviour of moving declarations to the top

a = 5;
console.log(a);
var a //let or const wont work here, hoisting



/*MULTIPLE VARIABLE DECLARATION*/
// all will be let or const

/*DATA TYPES*/

/*STRINGS*/
// series of 
let country = "Philippines";
console.log(country);


/*CONCATENATING STRINGS*/
// combine strings with +
let province = "Metro Manila";
console.log(province+" "+country);

/*ESCAPE CHARACTER*/
// /n 
let mailAddress = "Metro Manila /n/n Philippines";
console.log(mailAddress);

// "" and '' are valid in JS

console.log("John's employees went home early.");
console.log('John\'s employees went home early');

/*NUMBERS*/
// integers/whole numbers
let headcount = 26;
console.log(headcount);

// decimal/fractions
let grade = 98.7;
console.log(grade);

// exp notation
let planetDistance = 2e10;
console.log(planetDistance);

// combining strings and numbers
console.log("John's grade last quarter is "+grade);

// Boolean
// state, true or false (or 1,0)
let isMarried = false;
let inGoodConduct = true;

console.log("isMarried: "+isMarried);
console.log("inGoodConduct: "+inGoodConduct);

// Array
// store multiple values (usually similar data types)
/*SYNTAX: let/const varName = [x1,x2,x3,x4,..,xn]*/
let grades = [98.7,92.1,90.2,94.6];
console.log(grades);

// Different Data Type Array
// not devised to use info in an array that makes sense together
let person =  ["John","Smith", 32, true];
console.log(person);

// Object Data Type
// mimic world objects
// creates complex data that contains related data
let personDetails = {
	fullName: "Juan Dela Cruz",
	age: 35,
	isMarrie: true,
	contact: ["0912345679","09987654321"],
	address:{
		houseNumber: "345",
		city: "Manila"
	}
};
console.log(personDetails);

// typeof Keywords
// not sure or wants to assure the variable type
// testing
console.log(typeof personDetails);

// Constant Array
// NO reassign
// can change elements and properties
const anime = ["Naruto","Slam Dunk","One Piece"];
console.log(anime);
anime[0] = "Akame ga Kill";
console.log(anime);

// Null Data Type
// 0 is a number
// "" is a string
// null is no value and no type
let number = 0;
let string = "";
console.log(number);
console.log(string);

let jowa = null;
console.log(jowa);