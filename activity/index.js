console.log("Hello World");

/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:




/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	
let firstName = "Jygs", lastName = "Goco", age = 23;
console.log("First Name: "+firstName);
console.log("Last Name: "+lastName);
console.log("Age: "+age);

let hobbies = [
	"Play video games",
	"Sleeping",
	"Make coffee"
];
console.log("Hobbies:");
console.log(hobbies);

let workAddress = {
	houseNumber: "1404",
	street: "Governor I. Santiago",
	city: "Valenzuela",
	province: "Metro Manila"
};
console.log("Work Address:");
console.log(workAddress);

	let fullName = firstName + " " + lastName;
	console.log("My full name is: " + fullName);

	let currentAge = age;
	console.log("My current age is: " + currentAge);
	
	let friends = ["Chandler","Bruce","Ross","Rachel","Phoebe","Joey"];
	console.log("My Friends are: ");
	console.log(friends);

	let profile = {
		username: "jygsgoco",
		fullName,
		age,
		isActive: true
	}
	console.log("My Full Profile: ");
	console.log(profile);

	let bffName = "Gabriel Lazaro";
	console.log("My bestfriend is: " + bffName);

	const lastLocation = "where the Northwind meets the sea.";
	// lastLocation = "Atlantic Ocean";
	console.log("I was found frozen: " + lastLocation);

